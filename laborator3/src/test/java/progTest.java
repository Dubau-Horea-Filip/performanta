
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class progTest {

    public prog prog;

    @Before
    public void setup() {
        prog = new prog(10);
        prog.setup();
    }

    @Test
    public void add() {
        BigDecimal sum = new  BigDecimal("45");
        Assert.assertEquals(sum, prog.sum());
    }

    @Test
    public void avg() {
        BigDecimal sum = new  BigDecimal("4.5");
        Assert.assertEquals(sum, prog.avg());
    }



    @Test
    public void top() {
        Stream<BigDecimal> top;
        List<BigDecimal> list = new ArrayList<>();

        // Add element in list
        list.add(new BigDecimal("9"));


        // Get the Stream from the List

        Assert.assertEquals(list, prog.top10());
    }

    @Test
    public void Ser() throws IOException, ClassNotFoundException {
        for (int i = 0; i < 10; i++) {
            BigDecimal b = prog.list.get(i);
            byte[] a = prog.serialize(prog.list.get(i));
            BigDecimal c = prog.deserialize(a);
            Assert.assertEquals(b,c);
        }
    }








}
