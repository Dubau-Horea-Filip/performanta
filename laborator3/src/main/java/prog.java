

import java.io.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertThat;

public class prog {
    public prog() {
    }

    public prog(int size) {
        this.size = size;
    }

    List<BigDecimal> list = new ArrayList<>();
    List<byte[]> listSer = new ArrayList<>();

    Integer size = 20000;

    public void setup() {
        for (int i = 0; i < size; i++)
            list.add(new BigDecimal(i));
    }

    public void print() {
        for (int i = 0; i < size; i++)
            System.out.println(list.get(i));
    }


    public BigDecimal sum() {
        return this.list.stream().reduce(BigDecimal.valueOf(0), BigDecimal::add);
        //BigDecimal sum = new BigDecimal(“0”);
//        BigDecimal sum  = BigDecimal.valueOf(0);
//        for (int i = 0; i < size; i++)
//            sum = sum.add(list.get(i));
//
//        return sum;
    }


    public BigDecimal avg() {
        return this.list.stream().reduce(new BigDecimal(0), BigDecimal::add).divide(BigDecimal.valueOf(this.list.size()));
//        BigDecimal sum  = BigDecimal.valueOf(0);
//        for (int i = 0; i < size; i++)
//            sum = sum.add(list.get(i));
//
//        return sum.divide(new BigDecimal(size));
    }


    //– Serialize/Deserialize (as objects) the BigDecimals used
    //• Each item (BigDecimal) and not the entire container
    //• Consider a use case with 100M serialized elements that need be
    //processed when designing your code

    public void serialization() throws IOException {
        for (int i = 0; i < size; i++) {
            //list.set(i,)
            listSer.add(serialize(list.get(i)));
        }
    }

    public void deserialization() throws IOException, ClassNotFoundException {
        for (int i = 0; i < size; i++) {
            list.set(i, deserialize(listSer.get(i)));

        }
    }

    public List<BigDecimal> top10() {
        //Print the top 10% biggest numbers from the list using streams
        //list.stream().sorted(BigDecimal::compareTo).limit(this.size/10).forEach(System.out::println);
        return list.stream().sorted(Collections.reverseOrder()).limit(this.size / 10).toList();
    }

    public byte[] serialize(BigDecimal big) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ObjectOutputStream outputStream = new ObjectOutputStream(byteArrayOutputStream);
        outputStream.writeObject(big);
        return byteArrayOutputStream.toByteArray();
    }

    public BigDecimal deserialize(byte[] bytes) throws IOException, ClassNotFoundException {
        ObjectInputStream inputStream = new ObjectInputStream(new ByteArrayInputStream(bytes));
        return (BigDecimal) inputStream.readObject();
    }

}




