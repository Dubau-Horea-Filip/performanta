

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

public class BigDecimalExample {

    static void start() throws IOException, ClassNotFoundException {
        prog prog = new prog();
        prog.setup();
        prog.serialization();
        //main.prog.print();
        System.out.println("sum is: " + prog.sum());
        System.out.println("avg is: " + prog.avg());
        List<BigDecimal> top = prog.top10();
        System.out.println("top 10  is: " );
        top.forEach(System.out::println);
        prog.deserialization();
       // main.prog.top10();
    }

    public static void main(String[] args) throws IOException, ClassNotFoundException {

        start();
//        // Create two new BigDecimals
//        BigDecimal bd1 =
//                new BigDecimal("124567890.0987654321");
//        BigDecimal bd2 =
//                new BigDecimal("987654321.123456789");
//
//        // Addition of two BigDecimals
//        bd1 = bd1.add(bd2);
//        System.out.println("BigDecimal1 = " + bd1);
//
//        // Multiplication of two BigDecimals
//        bd1 = bd1.multiply(bd2);
//        System.out.println("BigDecimal1 = " + bd1);
//
//        // Subtraction of two BigDecimals
//        bd1 = bd1.subtract(bd2);
//        System.out.println("BigDecimal1 = " + bd1);
//
//        // Division of two BigDecimals
//        bd1 = bd1.divide(bd2);
//        System.out.println("BigDecimal1 = " + bd1);
//
//        // BigDecima1 raised to the power of 2
//        bd1 = bd1.pow(2);
//        System.out.println("BigDecimal1 = " + bd1);
//
//        // Negate value of BigDecimal1
//        bd1 = bd1.negate();
//        System.out.println("BigDecimal1 = " + bd1);
    }
}        