package com.example.Operations;

import com.example.Values.IntegerValue;

public class ArithmeticOperation implements Operation {

    private Operation<IntegerValue> value1;
    private Operation<IntegerValue> value2;
    private String operation;

    public ArithmeticOperation(Operation<IntegerValue> v1, String op, Operation<IntegerValue> v2){
        this.value1 = v1;
        this.value2 = v2;
        this.operation = op;
    }

    @Override
    public IntegerValue run() throws Exception {
        switch(this.operation){
            case "+":
                return new IntegerValue(value1.run().getValue() + value2.run().getValue());
            case "-":
                return new IntegerValue(value1.run().getValue() - value2.run().getValue());
            case "*":
                return new IntegerValue(value1.run().getValue() * value2.run().getValue());
            case "/":
                if(value2.run().getValue() != 0)
                    return new IntegerValue(value1.run().getValue() / value2.run().getValue());
                throw new Exception("Division by zero");
            case "min":
                if(value1.run().getValue() < value2.run().getValue())
                    return value1.run();
                return value2.run();
            case "max":
                if(value1.run().getValue() > value2.run().getValue())
                    return value1.run();
                return value2.run();
            default:
                break;
        }
        return null;
    }

}

