package com.example.Operations;

import com.example.Values.IntegerValue;

public class Operation_Self implements Operation<IntegerValue> {
    private Operation<IntegerValue> value1;
    private String operation;

    public Operation_Self(String op, Operation<IntegerValue> v1) {
        this.value1 = v1;
        this.operation = op;
    }

    @Override
    public IntegerValue run() throws Exception {
        switch (this.operation) {
            case "sqrt":
                return new IntegerValue((int) Math.sqrt(value1.run().getValue()));
            default:
                throw new Exception("Unsupported operation " + operation);
        }
    }
}

