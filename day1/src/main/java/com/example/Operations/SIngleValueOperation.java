package com.example.Operations;

import com.example.Values.IntegerValue;

public class SIngleValueOperation implements Operation<IntegerValue>{

    private final IntegerValue value ;

    public SIngleValueOperation(IntegerValue value)
    {
        this.value = value;
    }



    @Override
    public IntegerValue run() throws Exception {
        return value;
    }
}
