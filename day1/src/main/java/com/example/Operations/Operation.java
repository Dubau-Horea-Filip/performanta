package com.example.Operations;

import com.example.Values.IntegerValue;

public interface Operation<T> {
    public T run() throws Exception;
}
