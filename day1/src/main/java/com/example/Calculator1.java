package com.example;


import com.example.Operations.Operation;
import com.example.Values.IntegerValue;

public class Calculator1 {
    public static Integer calculate(Operation<IntegerValue> input){
        try {
            return input.run().getValue();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
        return -1;
    }
}
