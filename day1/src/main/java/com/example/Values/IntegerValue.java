package com.example.Values;

public class IntegerValue implements Value {
    private Integer value;

    public IntegerValue(Integer value) {
        this.value = value;
    }


    public Integer getValue() {
        return this.value;
    }


    public void setValue(Integer value) {
        this.value = value;
    }

}
