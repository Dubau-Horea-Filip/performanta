package com.example;

import java.util.Scanner;

public class Calculator {

    public void max()
    {
        Scanner scan = new Scanner(System.in);
        System.out.println("1'st number: ");
        int nr1 = scan.nextInt();
        System.out.println("2'nd number: ");
        int nr2 = scan.nextInt();
        int max = Math.max(nr1,nr2);
        System.out.println(max);
    }

    public void add() {
        Scanner scan = new Scanner(System.in);
        System.out.println("1'st number: ");
        int nr1 = scan.nextInt();
        System.out.println("2'nd number: ");
        int nr2 = scan.nextInt();
        int sum = nr1 + nr2;
        System.out.println(sum);
    }

    public void subtraction() {
        Scanner scan = new Scanner(System.in);
        System.out.println("1'st number: ");
        int nr1 = scan.nextInt();
        System.out.println("2'nd number: ");
        int nr2 = scan.nextInt();
        int sub = nr1 - nr2;
        System.out.println(sub);
    }

    public void multiply() {
        Scanner scan = new Scanner(System.in);
        System.out.println("1'st number: ");
        int nr1 = scan.nextInt();
        System.out.println("2'nd number: ");
        int nr2 = scan.nextInt();
        int sub = nr1 * nr2;
        System.out.println(sub);
    }

    public void devide() {
        Scanner scan = new Scanner(System.in);
        System.out.println("1'st number: ");
        int nr1 = scan.nextInt();
        System.out.println("2'nd number: ");
        int nr2 = scan.nextInt();
        int sub = nr1 / nr2;
        System.out.println(sub);
    }


    public static void menu() {
        System.out.println("\nmenu");
        System.out.println("0:exit");
        System.out.println("1:add 2 numbers");
        System.out.println("2:subtract 2 numbers");
        System.out.println("3:multiply 2 numbers");
        System.out.println("4:divide 2 numbers");
    }

    public void start() {
        boolean ok = true;
        Scanner scan = new Scanner(System.in);
        while (ok) {
            menu();
            int option = scan.nextInt();
            switch (option) {
                case 1:
                    add();
                    break;
                case 2:
                    subtraction();
                    break;
                case 3:
                    multiply();
                    break;
                case 4:
                    devide();
                    break;
                case 0:
                    ok = false;
                    break;

            }
        }
    }
}
