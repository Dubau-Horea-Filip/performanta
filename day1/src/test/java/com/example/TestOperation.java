package com.example;

import com.example.Operations.ArithmeticOperation;
import com.example.Operations.Operation;

import com.example.Operations.Operation_Self;
import com.example.Operations.SIngleValueOperation;
import com.example.Values.IntegerValue;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestOperation {
//[(5+4)-sqrt(4)]/2 =3
    Operation<IntegerValue> op = new ArithmeticOperation(
                new ArithmeticOperation(
            new ArithmeticOperation(
                    new SIngleValueOperation(new IntegerValue(5)),"+", new SIngleValueOperation(new IntegerValue(4)))
            , "-",
            new Operation_Self("sqrt",new SIngleValueOperation(new IntegerValue(4)))), "/", new SIngleValueOperation(new IntegerValue(2)));
    IntegerValue val;

    //assertEquals(i, result);3

    @Test
    public void test() {
        {



            try {
                Integer i = 3;
                val = new IntegerValue(op.run().getValue());
                System.out.println(val.getValue());
                assertEquals (i,val.getValue());
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }



}
