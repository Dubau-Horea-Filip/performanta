package com.Repositories;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ConcurrentHashMapBassedRepository<K,V> implements KeyValueRepository<K,V> {
    private Map<K,V> map;

    public ConcurrentHashMapBassedRepository() {
        this.map = new ConcurrentHashMap<>();
    }

    @Override
    public void add(K key, V value) {
        map.put(key,value);
    }

    @Override
    public boolean contains(K key, V value) {
        return map.containsKey(key) && map.containsValue(value);
    }

    @Override
    public void remove(K key, V value) {
        map.remove(key);
    }
}
