package com.Repositories;


import gnu.trove.map.TMap;
import gnu.trove.map.hash.THashMap;

public class TroveRepository<K, V> implements KeyValueRepository<K, V> {
    TMap<K, V> map;

    public TroveRepository() {
        this.map = new THashMap<>();
    }

    @Override
    public void add(K key, V value) {
        this.map.put(key, value);
    }

    @Override
    public boolean contains(K key, V value) {
        return this.map.containsKey(key) && this.map.containsValue(value);
    }

    @Override
    public void remove(K key, V value) {
        this.map.remove(key, value);
    }
}