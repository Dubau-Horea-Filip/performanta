package com.Repositories;

public interface KeyValueRepository<K,V> {
    void add(K key, V value);
    boolean contains(K key, V value);
     void remove(K key, V value);

}
