package com.Repositories;


import org.eclipse.collections.api.map.MutableMap;
import org.eclipse.collections.impl.map.mutable.UnifiedMap;
import org.eclipse.collections.impl.tuple.Tuples;

public class EclipseRepository<K,V> implements KeyValueRepository<K,V> {
    MutableMap<K,V> map;

    public EclipseRepository() {
        this.map = new UnifiedMap<>();
    }

    @Override
    public void add(K key, V value) {
        this.map.add(Tuples.pair(key, value));
    }

    @Override
    public boolean contains(K key, V value) {
        return this.map.contains(Tuples.pair(key,value));
    }

    @Override
    public void remove(K key, V value) {
        this.map.remove(key,value);
    }
}
