package com.Repositories;

public interface InMemoryRepository<T> {
    public void add(T item);
    public boolean contains(T item);
    public void remove(T item);
}
