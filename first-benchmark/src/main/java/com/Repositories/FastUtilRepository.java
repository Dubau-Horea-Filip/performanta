package com.Repositories;



import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectList;

public class FastUtilRepository<T> implements InMemoryRepository<T> {
    ObjectList<T> list;

    public FastUtilRepository() {
        this.list = new ObjectArrayList<>();
    }

    @Override
    public void add(T elem) {
        this.list.add(elem);
    }

    @Override
    public boolean contains(T elem) {
        return this.list.contains(elem);
    }

    @Override
    public void remove(T elem) {
        this.list.remove(elem);
    }
}
