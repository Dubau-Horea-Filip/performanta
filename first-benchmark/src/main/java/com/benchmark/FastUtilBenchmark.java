
package com.benchmark;


import com.Order;
import com.Repositories.FastUtilRepository;
import com.Repositories.InMemoryRepository;
import org.openjdk.jmh.annotations.*;

import java.util.Random;
import java.util.concurrent.TimeUnit;

@State(Scope.Benchmark)
@BenchmarkMode(Mode.Throughput)
@OutputTimeUnit(TimeUnit.SECONDS)
@Fork(2)
@Warmup(iterations = 3, time = 1, timeUnit = TimeUnit.SECONDS)
@Measurement(iterations = 3, time = 1, timeUnit = TimeUnit.SECONDS)
@Threads(1)
public class FastUtilBenchmark {

    @State(Scope.Benchmark)
    public static class ObjectArrayListBenchmarkSetup {
        public InMemoryRepository<Order> repository;
        private final int size = 100000;

        @Setup(Level.Trial)
        public void setUp() {
            repository = new FastUtilRepository<>();
            for (int i = 0; i < size; i++)
                repository.add(new Order(i, i, i));
        }

        @TearDown(Level.Trial)
        public void tearDown() {
            repository = null;
        }
    }

    @Benchmark
    public void add(final ObjectArrayListBenchmarkSetup state) {
        int i = new Random().nextInt(state.size);
        state.repository.add(new Order(i, i, i));
    }

    @Benchmark
    public void contains(final ObjectArrayListBenchmarkSetup state) {
        int i = new Random().nextInt(state.size);
        state.repository.contains(new Order(i, i, i));
    }

    @Benchmark
    public void remove(final ObjectArrayListBenchmarkSetup state) {
        int i = new Random().nextInt(state.size);
        state.repository.remove(new Order(i, i, i));
    }
}
