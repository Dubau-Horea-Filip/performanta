package com.benchmark;

import com.Order;
import com.Repositories.ConcurrentHashMapBassedRepository;
import com.Repositories.KeyValueRepository;
import org.openjdk.jmh.annotations.*;

import java.util.Random;
import java.util.concurrent.TimeUnit;

@State(Scope.Benchmark)
@BenchmarkMode(Mode.Throughput)
@OutputTimeUnit(TimeUnit.SECONDS)
@Fork(2)
@Warmup(iterations = 3, time = 1, timeUnit = TimeUnit.SECONDS)
@Measurement(iterations = 3, time = 1, timeUnit = TimeUnit.SECONDS)
@Threads(1)
public class ConcurrentHashMapBenchmark {

    @State(Scope.Benchmark)
    public static class ConcurrentHashMapBenchmarkSetup {
        public KeyValueRepository<Integer, Order> repository;
        private final int size = 100000;

        @Setup(Level.Trial)
        public void setUp() {
            repository = new ConcurrentHashMapBassedRepository<>();
            for (int i = 0; i < size; i++)
                repository.add(i, new Order(i, i, i));
        }

        @TearDown(Level.Trial)
        public void tearDown() {
            repository = null;
        }
    }

    @Benchmark
    public void add(final ConcurrentHashMapBenchmarkSetup state) {
        int i = new Random().nextInt(state.size);
        state.repository.add(i, new Order(i, i, i));
    }

    @Benchmark
    public void contains(final ConcurrentHashMapBenchmarkSetup state) {
        int i = new Random().nextInt(state.size);
        state.repository.contains(i, new Order(i, i, i));
    }

    @Benchmark
    public void remove(final ConcurrentHashMapBenchmarkSetup state) {
        int i = new Random().nextInt(state.size);
        state.repository.remove(i, new Order(i, i, i));
    }
}
