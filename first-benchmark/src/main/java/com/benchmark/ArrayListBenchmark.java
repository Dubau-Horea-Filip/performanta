
package com.benchmark;


import com.Order;
import com.Repositories.ArrayListBassedRepository;
import com.Repositories.InMemoryRepository;

import org.openjdk.jmh.annotations.*;

import java.util.Random;
import java.util.concurrent.TimeUnit;

@State(Scope.Benchmark)
@BenchmarkMode(Mode.Throughput)
@OutputTimeUnit(TimeUnit.SECONDS)
@Fork(2)
@Warmup(iterations = 4, time = 1, timeUnit = TimeUnit.SECONDS)
@Measurement(iterations = 2, time = 1, timeUnit = TimeUnit.SECONDS)
@Threads(1)
public class ArrayListBenchmark {

    @State(Scope.Benchmark)
    public static class ArrayListBenchmarkSetup {
        public InMemoryRepository<Order> repository;
        public InMemoryRepository<Order> repository_init;
        private final int size = 100000;

        @Setup(Level.Trial)
        public void setUp() {
            repository = new ArrayListBassedRepository<>();
            for (int i = 0; i < size; i++)
                repository_init.add(new Order(i, i, i));
        }

        @TearDown(Level.Trial)
        public void tearDown() {
            repository = null;
        }
    }

    @Benchmark
    public void add(final ArrayListBenchmarkSetup state) {
        int i = new Random().nextInt(state.size);
        state.repository.add(state.repository.get(i));
    }

    @Benchmark
    public void contains(final ArrayListBenchmarkSetup state) {
        int i = new Random().nextInt(state.size);
        state.repository.contains(new Order(i, i, i));
    }

    @Benchmark
    public void remove(final ArrayListBenchmarkSetup state) {
        int i = new Random().nextInt(state.size);
        state.repository.remove(new Order(i, i, i));
    }
}
