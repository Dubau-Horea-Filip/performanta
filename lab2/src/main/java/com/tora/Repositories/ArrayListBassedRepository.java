package com.tora.Repositories;

import java.util.ArrayList;
import java.util.List;

public class ArrayListBassedRepository<T> implements InMemoryRepository<T> {
    public List<T> list = new ArrayList<T>();


    @Override
    public void add(T item) {
        list.add(item);
    }

    @Override
    public boolean contains(T item) {
        return list.contains(item);
    }

    @Override
    public void remove(T item) {
        list.remove(item);
    }
}
