package com.tora.Repositories;

public interface InMemoryRepository<T> {
    public void add(T item);
    public boolean contains(T item);
    public void remove(T item);
}
